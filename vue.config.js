// const path = require('path');
// const PrerenderSpaPlugin = require('prerender-spa-plugin');
// const Renderer = PrerenderSpaPlugin.PuppeteerRenderer;

const CompressionWebpackPlugin = require('compression-webpack-plugin');
// const productionGzipExtensions = ['js', 'css'];

module.exports = {
  outputDir: "dist",
  assetsDir: "assets",
  productionSourceMap: false,
  devServer: {
    proxy: {
      '/wp-json': {
        target: 'http://207.148.115.180:8080/',
        changeOrigin: true,
        ws: true,
        secure: false,
        cookieDomainRewrite: 'localhost',
        // 把cookie的secure去掉
        onProxyRes: (proxyResponse) => {
          if (proxyResponse.headers['set-cookie']) {
            const cookies = proxyResponse.headers['set-cookie'].map(cookie => cookie.replace(/; secure/gi, ''));
            proxyResponse.headers['set-cookie'] = cookies;
          }
        },
      },
    },
    disableHostCheck: true,
    // host: 'abcc.cmss.us',
  },
  css: {
    // 将组件内部的css提取到一个单独的css文件（只用在生产环境）

    // 也可以是传递给 extract-text-webpack-plugin 的选项对象

    extract: true, // 允许生成 CSS source maps?

    sourceMap: false, // pass custom options to pre-processor loaders. e.g. to pass options to // sass-loader, use { sass: { ... } }

    loaderOptions: {}, // Enable CSS modules for all css / pre-processor files. // This option does not affect *.vue files.

    modules: false
  }, // use thread-loader for babel & TS in production build // enabled by default if the machine has more than 1 cores

  configureWebpack: {
    devtool: "inline-source-map",
    plugins: [
      // SEO预处理
      // new PrerenderSpaPlugin(
      // Absolute path to compiled SPA
      // path.resolve(__dirname, 'dist'),
      // List of routes to prerender
      // ['/', '/home'],
      // ),
      // gzip压缩
      new CompressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(js|css)$'),
        threshold: 10240,
        minRatio: 0.8
      })
    ],
  }
}