import Vue from 'vue';
import Router from 'vue-router';

import MyHome from '@/components/MyHome';
import MyProduct from '@/components/product';
import productList from '@/components/productList';
import solution from '@/components/solution';
import contact from '@/components/contact';
import about from '@/components/about';
import ie from '@/ie';
import news from '@/components/news';
import prodcutAll from '@/components/productAll';

Vue.use(Router);

export default new Router({
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return {
      x: 0,
      y: 0,
    };
  },
  routes: [{
    path: '/',
    name: 'MyHome',
    component: MyHome,
  }, {
    path: '/product/',
    component: productList,
  }, {
    path: '/product/:id',
    name: 'product',
    component: MyProduct,
  }, {
    path: '/solution',
    name: 'solution',
    component: solution,
  }, {
    path: '/solution/:id',
    name: 'solution',
    component: MyProduct,
  }, {
    path: '/service/:id',
    name: 'service',
    component: MyProduct,
  }, {
    path: '/ie',
    name: 'ie',
    component: ie,
  }, {
    path: '/contact/:id',
    name: 'contact',
    component: contact,
  }, {
    path: '/about/:id',
    name: 'about',
    component: about,
  }, {
    path: '/productAll',
    name: 'productAll',
    component: prodcutAll
  }, {
    path: '/news/:id',
    name: 'news',
    component: news

  }],
});