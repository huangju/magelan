import 'babel-polyfill';
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import axios from 'axios';
import MetaInfo from 'vue-meta-info';
import router from './router';
import App from './App.vue'
import './wordpress.css';
import {
  personImg,
  solution,
  service,
} from './components/config';

const baseURL = 'http://207.148.115.180:8080/';
// const emaliURL = 'http://localhost:3000';

Vue.config.productionTip = false;

Vue.use(MetaInfo);
Vue.use(VueAnalytics, {
  id: 'UA-130435257-1',
  router,
})

Vue.prototype.$api = axios.create({
  baseURL,
  validateStatus(status) {
    return status < 500;
  },
});

Vue.prototype.$ajax = axios.create({
  // emaliURL,
  validateStatus(status) {
    return status < 500;
  },
});


new Vue({
  router,
  data() {
    return {
      service,
      solution,
      personImg,
    }
  },
  render: h => h(App),
}).$mount('#app')