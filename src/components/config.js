export const personImg = [{
    id: 54,
    url: require("../assets/product-1.jpg"),
    title: "Bottled Water Filling Machine"
  },
  {
    id: 63,
    url: require("../assets/product-2.jpg"),
    title: "Carbonated Beverage Production Equipment"
  },
  {
    id: 65,
    url: require("../assets/product-3.jpg"),
    title: "Coca-Cola Bottling Production Line"
  },
  {
    id: 70,
    url: require("../assets/product-4.jpg"),
    title: "Condiment Filling Machine"
  },
  {
    id: 74,
    url: require("../assets/product-5.jpg"),
    title: "Daily Chemical Packing Machinery"
  },
  {
    id: 76,
    url: require("../assets/product-6.jpg"),
    title: "Drinking Water Filling Machine"
  },
  {
    id: 78,
    url: require("../assets/product-7.jpg"),
    title: "Juice Filling Machine"
  },
  {
    id: 80,
    url: require("../assets/product-8.jpg"),
    title: "Non-gas Beverage Filling Machine Line"
  },
  {
    id: 82,
    url: require("../assets/product-9.jpg"),
    title: "Pure Water Filling Line"
  },
  {
    id: 84,
    url: require("../assets/product-10.jpg"),
    title: "Tea (Milk) Drinks Filling Machine"
  },
  {
    id: 86,
    url: require("../assets/product-11.jpg"),
    title: "Water Bottling Machine"
  },
  {
    id: 88,
    url: require("../assets/product-12.jpg"),
    title: "Water Bottling Plant"
  }
];

export const solution = [{
  id: 118,
  title: 'Beer filling machine',
}, {
  id: 129,
  title: 'Bottled Water Filling Machine',
}, {
  id: 132,
  title: 'Juice Filling Line',
}];

export const service = [{
  id: 195,
  title: 'Technology Service',
},{
  id: 197,
  title: 'After-Sale Service',
}, {
  id: 203,
  title: 'Common-Problems'
}]